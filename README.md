ldstatic
========

To run simply do `sh build.sh foaf.rdf http://lmatteis.github.io/ldstatic/ldf/`. It will parse the file and create Turtle pages inside the `ldf/` directory.
